/* lib/lib_hwtest1.c */

#include <stdio.h>
#define LIB_HW_PRIVATE_PROTOTYPES
#include "hw-cmake.h"
#undef LIB_HW_PRIVATE_PROTOTYPES

#include "internal-cmake.h"

void receive_byte () {
   printf ("Hello, receive_byte\n");
   /* call internal library */
   internal_receive_byte();
}
